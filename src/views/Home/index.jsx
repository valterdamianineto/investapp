import { Container, CardBox, LeftSide, RightSide } from './styles'
import Header from '../../components/Header'
import LeftCard from '../../components/LeftCard'
import RightCard from '../../components/RightCard'

function Home() {
  return (
      <Container>
        <Header title="Simulador de Investimentos"/>
        <Header subtitle="Simulador"/>
        <CardBox>
            <LeftSide>
              <LeftCard/>
              <RightCard/>
            </LeftSide>
            <RightSide>
            </RightSide>
        </CardBox>
      </Container>
  );
}

export default Home;
