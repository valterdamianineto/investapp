import styled from "styled-components";

export const Container = styled.div`
    background-color: #efefef;
    width: 96%;
    height: 95vh;
    margin-left: 2%;
`
export const CardBox = styled.div`
    display: flex;
`

export const LeftSide = styled.div`
    width: 50%;
    display: flex;
`

export const RightSide = styled.div`
    background-color: orange;
    width: 50%
`