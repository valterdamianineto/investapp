import React, { useState } from 'react';
import { Container, Label, InputBox, Input, Message, Symbol } from "./styles";

const Field = ({label, inputType}) => {
    
    const [fieldValue, setFieldValue] = useState('');
    const [fieldMessage, setFieldMessage] = useState('');

    const validateField = (inputType, value) => {
        if(inputType === 'money') {
            let removeFormat = value.replace(".","").replace(",","")
            let isValid = /^\d+$/.test(removeFormat)
            if (isValid) {
                setFieldMessage('')
                setFieldValue(formatCurrency(value))
            }else {
                setFieldValue(value)
                setFieldMessage('Aporte Inicial deve ser um número')
                if(value === '') {
                    setFieldMessage('')
                }
            }
        }

        if(inputType === 'percent') {
            let removeFormat = value.replace(",","")
            let isValid = /^\d+$/.test(removeFormat)
            if (isValid) {
                setFieldMessage('')
                setFieldValue(formatPercent(value))
            }else {
                setFieldValue(value)
                setFieldMessage('IPCA deve ser um número')
                if(value === '') {
                    setFieldMessage('')
                }
            }
        }

        if(inputType === 'month') {
            let removeFormat = value.replace(",","")
            let isValid = /^\d+$/.test(removeFormat)
            if (isValid) {
                setFieldMessage('')
                setFieldValue(value)
            }else {
                setFieldValue(value)
                setFieldMessage('Prazo deve ser um número')
                if(value === '') {
                    setFieldMessage('')
                }
            }
        }
    }
    
    function formatCurrency(inputValue) {
        inputValue = parseInt(inputValue.replace(/[\D]+/g, ''));
        inputValue = inputValue + '';
        inputValue = inputValue.replace(/([0-9]{2})$/g, ",$1");

        if (inputValue.length > 6) {
            inputValue = inputValue.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
        }
        return inputValue
    }

    function formatPercent(inputValue) {
        inputValue = parseInt(inputValue.replace(/[\D]+/g, ''));
        inputValue = inputValue + '';
        inputValue = inputValue.replace(/([0-9]{2})$/g, ",$1");
        return inputValue
    }

    return (
        <Container>
            <Label redColor={fieldMessage}>{label}</Label>
            <InputBox redColor={fieldMessage}>
                {inputType === 'money' ? <Symbol>R$</Symbol> : null }
                <Input
                    onChange={e => validateField(inputType, e.target.value)}
                    value={fieldValue}
                />
                {inputType === 'percent' ? <Symbol>%</Symbol> : null }
            </InputBox>
            <Message>{fieldMessage}</Message>
        </Container>
    )
}

export default Field;