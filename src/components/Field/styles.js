import styled from "styled-components";

export const Container = styled.div``;

export const Label = styled.p`
    text-align: left;
    width: 100%;
    font-size: 0.8rem;
    padding: 1rem 0;
    margin: 0;
    color: ${props => props.redColor ? "#ff5252" : "black"}
`

export const InputBox = styled.div`
    display: flex;
    padding: 0.3rem;
    border-bottom: 0.1rem solid ${props => props.redColor ? "#ff5252" : "black"};
`
export const Symbol = styled.span`
    margin-right: 0.2rem
`

export const Input = styled.input`
    border: none;
    font-size: 1rem;
    background-color: transparent;
    width: 100%;
    &:focus {
        outline: none;
        box-shadow: none;
    }
`

export const Message =  styled.span`
    color: #ff5252 !important;
    font-size: 0.7rem;
`
