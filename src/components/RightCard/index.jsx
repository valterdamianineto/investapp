import { Container } from "./styles";
import CardHeader from '../CardHeader'
import Field from '../Field'
import TriploSelector from '../TripleSelector'

const RightCard = () => (
    <Container>
        <CardHeader
            title="Tipos de indexação"
            showIcon={true}
        />
        <TriploSelector
            left='pré'
            middle='pos'
            right='fixado'/>
        <Field label="Aporte Inicial" inputType="money"/>
        <Field label="Rentabilidade" inputType="percent"/>
        <Field label="CDI (ao ano)" inputType="percent"/>
    </Container>
)

export default RightCard;
