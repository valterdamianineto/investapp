import React, { useState } from 'react';
import { Container, LeftSide, RightSide } from "./styles";
import Checked from '../../assets/check'

const DoubleSelector = (props) => {
    const [clickedValue, setClicked] = useState('');
    const [clickSide, setSide] = useState('left');
    
    function setValue(value, side) {
        setSide(side)
        setClicked(value)
    }

    return (
        <Container>
            <LeftSide
                clicked={clickSide}
                onClick={() => setValue(props.left, 'left')}
            >
            {clickSide === 'left' ?
                <Checked/> : null
            }{props.left}</LeftSide>
            <RightSide
                clicked={clickSide}
                onClick={() => setValue(props.right, 'right')}
            >
            {clickSide === 'right' ?
                <Checked/> : null
            }{props.right}</RightSide>
        </Container>
    )
}

export default DoubleSelector;