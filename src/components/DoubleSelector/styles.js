import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    margin: 0.8rem 0;
`

export const LeftSide = styled.button`
    border-top-left-radius: 10px;
    border-bottom-left-radius: 10px;
    border: 0.1rem solid black;
    text-transform: capitalize;
    text-align: center;
    padding: 1rem 0;
    cursor: pointer;
    width: 50%;
    background: ${props => props.clicked === 'left' ? '#ed8e53' : 'transparent'};
    color: ${props => props.clicked === 'left' ? 'white' : 'black'};
    display: flex;
    align-items: center;
    justify-content: center;
    &:hover {
        background: #ffaa75;
        color: white;
    }
`
    
export const RightSide = styled.button`
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
    border: 0.1rem solid black;
    text-transform: capitalize;
    text-align: center;
    padding: 1rem 0;
    cursor: pointer;
    width: 50%;
    background: ${props => props.clicked === 'right' ? '#ed8e53' : 'transparent'};
    color: ${props => props.clicked === 'right' ? 'white' : 'black'};
    display: flex;
    align-items: center;
    justify-content: center;
    &:hover {
        background: #ffaa75;
        color: white;
    }
`