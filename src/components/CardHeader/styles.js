import styled from "styled-components";

export const Container = styled.div`
    padding: 0;
    width: 100%;
`;

export const TitleBox = styled.div`
    display: flex;
    align-items: center;
    padding: 0;
    justify-content: space-between;
`;

export const Title = styled.p`
    font-size: 15px;
    text-align: left;
    margin: 0;
`;