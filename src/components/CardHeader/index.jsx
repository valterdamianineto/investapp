import { Container, TitleBox, Title } from "./styles";
import ExclamationCircle from '../../assets/exclamationCircle'

const CardHeader = (props) => (
    <Container>
        <TitleBox>
            <Title>{props.title}</Title>
            {props.showIcon ?
                <ExclamationCircle/> : null
             }
        </TitleBox>
    </Container>
)

export default CardHeader;
