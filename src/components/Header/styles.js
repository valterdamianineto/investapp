import styled from "styled-components";

export const Container = styled.div``

export const Title = styled.h1`
    padding-top: 1rem;
`

export const Subtitle = styled.h2`
    text-align: left;
    font-size: 1.5rem;
    padding: 0 1rem;
`