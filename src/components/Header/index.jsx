import { Container, Title, Subtitle } from "./styles";

const Header = (props) => (
    <Container>
        <Title>{props.title}</Title>
        <Subtitle>{props.subtitle}</Subtitle>
    </Container>
)

export default Header;