import { Container } from "./styles";
import CardHeader from '../CardHeader'
import Field from '../Field'
import DoubleSelector from "../DoubleSelector";

const LeftCard = () => (
    <Container>
        <CardHeader
            title="Rendimento"
            showIcon={true}
        />
        <DoubleSelector
            left='bruto'
            right='líquido'
        />
        <Field label="Aporte Inicial" inputType="money"/>
        <Field label="Prazo (em meses)" inputType="month"/>
        <Field label="IPCA (ao ano)" inputType="percent"/>
    </Container>
)

export default LeftCard;
